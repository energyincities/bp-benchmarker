padd_uneven <- function(L) {
  pad.na <- function(x,len) {
    c(x,rep(NA,len-length(x)))
  }
  maxlen <- max(sapply(L,length))
  do.call(data.frame,lapply(L,pad.na,len=maxlen))
}

get_proflik <- function(x, model, var, X) {
  model$ParameterValues[var, "initial"] <- x
  model$ParameterValues[var, "type"] <- 'fixed'
  capture.output(fit <- model$estimate(data=X), file='/dev/null')
  loglik <- fit$loglik
  if (is.null(loglik)){
    # a return of 0 means this was a failure
    loglik <- 0
  }
  return(loglik)
}


profile_likelihoods <- function(ranges, model_id, X, save_dir) {
  profs_fname <- file.path(save_dir, paste('profliks', 'csv', sep='.'))
  # prepare variables for likelihood profiling
  vars  <- names(ranges)
  model <- prep_model(mid = model_id)
  capture.output(fit <- model$estimate(data=X), file='/dev/null')
  loglik <- fit$loglik
  profliks <- list()
  for(var in vars) {
    range = ranges[[var]]
    model <- prep_model(mid = model_id)
    proflik <- c()
    for(x in range) {
      proflik <- c(proflik, get_proflik(x, model, var, X))
    }
    profliks[[paste(var, 'plike' , sep='_')]] <- proflik
    profliks[[paste(var, 'range' , sep='_')]] <- range
    lrs <- -2 * (proflik - loglik)
    profliks[[paste(var, 'lratio', sep='_')]] <- lrs
  }
  profliks  <- padd_uneven(profliks)
  outputDat <- data.frame(profliks)
  write.csv(outputDat, profs_fname)
}
