#General packages
import numpy as np
import pandas as pd
import pickle
import time

#Packages for Surrogate model training & optimization-based calibration
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from tensorflow.keras import Sequential, optimizers, Model, models
from tensorflow.keras.layers import Dense, LeakyReLU, LocallyConnected1D, Permute, Conv2D
from tensorflow.keras.regularizers import l2

#Packages for Bayesian Calibration see https://pypi.org/project/pymcmcstat/
import scipy.io as sio
from scipy.integrate import odeint
from pymcmcstat.MCMC import MCMC
from pymcmcstat.plotting import MCMCPlotting
np.seterr(over='ignore')


""
            ###Functions to perform Surrogate model training (step before calibration###
""
def build_model(input_dim, output_dim):
    model = Sequential()
    model.add(Dense(512, input_dim=input_dim,  kernel_regularizer=l2(1e-5)))
    model.add(LeakyReLU())
    model.add(Dense(output_dim, kernel_regularizer=l2(1e-5)))
    model.add(LeakyReLU())
    adam = optimizers.Adam(lr=1e-3)
    model.compile(optimizer=adam, loss='mean_squared_error')
    return model

def fit_surrogate(path):
    outputs = pd.read_csv('heating_rate_'+path+'.csv', index_col='Unnamed: 0').values
    inputs = np.array(pd.read_csv('inputs_'+path+'.csv', index_col='Unnamed: 0').iloc[:,0].values).reshape(-1,1)
    ss = StandardScaler()
    X = ss.fit_transform(inputs)
    ss_out = StandardScaler()
    ss_out = ss_out.fit(outputs.ravel().reshape(-1,1))
    y = ss_out.transform(outputs)

    pickle.dump([ss, ss_out], open(f'normalizers'+path+'.p','wb'))
    
    model = build_model(len(X[0,:]), y.shape[1])
    
    model.fit(X,y, epochs=700, validation_split=0.2, verbose=False)
    
    print(r2_score(y.transpose(),model.predict(X).transpose()))
    model.save(f'surrogate' + path + '.h5')
    return model


""
                        ###Functions to perform Bayesian Calibration###
""
def run_bayesian_calibration(path):
    [ss, ss_out] = pickle.load(open(f'normalizers'+path+'.p','rb'))
    model = models.load_model(f'surrogate'+path+'.h5')
    
    def heatingdaily_ss(theta, y_true):
        # evaluate model
        y_hat = ss_out.inverse_transform(model.predict(ss.transform(theta.reshape(1,-1))))
    
        res = y_hat - y_true.ydata[0].ravel()
        #print(res.shape)
        sos = (res**2).sum()
        #print(sos)
        return sos    
    
    
    results=[]

    outputs = pd.read_csv(f'heating_rate_'+path+'.csv', index_col='Unnamed: 0')
    inputs = pd.read_csv(f'inputs_'+path+'.csv', index_col='Unnamed: 0').iloc[:,1:2]
    
    for i in range(0,200):
        outputs_test = outputs.iloc[i,:].values
            # initialize MCMC object
        mcstat = MCMC()
        # initialize data structure 
        mcstat.data.add_data_set(x=np.ones(1),
                                 y=outputs_test.ravel())
        # initialize parameter array
        for j in inputs.columns:
            mcstat.parameters.add_model_parameter(name=j, 
                                                  theta0=inputs[j].mean(), 
                                                  minimum = inputs[j].min(),
                                                  maximum=inputs[j].max(),
                                                  prior_mu=inputs[j].mean(),
                                                 )

        # Generate options
        mcstat.simulation_options.define_simulation_options(
            nsimu=3000, 
            burnin_scale=500,
            updatesigma=True,
            method='dram',
            results_filename=f'results_{i}',
            save_to_json=False)
        # Define model object:
        mcstat.model_settings.define_model_settings(
            sos_function=heatingdaily_ss,)
        mcstat.run_simulation()

            
    pd.DataFrame(np.array(results)).to_csv('bc_results_'+path+'.csv')



""
            ###Functions to perform Optimization-based calibration (GA/using Backprop###
""

def modify_model(model, input_dim):
    '''
    Modifies the trained model to use backpropagtion (instead of Genetic Algorithm) for model calibration.
    Therefore, we set the layers to non-trainable and add one single neuron layer to the beginning which represents the heat-loss coefficient to be calibrated.
    '''
    model_cali = Sequential()
    model_cali.add(LocallyConnected1D(1, 1, input_shape=input_dim, use_bias=False))
    model_cali.add(Permute((2,1)))
    for i in range(len(model.layers)):
        model_cali.add(model.layers[i])
        model_cali.layers[-1].trainable=False
    adam = optimizers.Adam(lr=1e-3)
    model_cali.compile(optimizer=adam, loss='mean_squared_error')
    return model_cali

def run_calibration(path):
    '''
    Perform backpropagation model calibration (chosen as it is much fast and even more accurate than GA).
    '''
    
    #Load in the normalizers & the trained surrogate model.
    [ss, ss_out] = pickle.load(open(f'normalizers'+path+'.p','rb'))
    model = models.load_model(f'surrogate'+path+'.h5')
    
    results=[]
    #Load in the daily time series data.
    outputs = pd.read_csv(f'heating_rate_'+path+'.csv', index_col='Unnamed: 0')
    
    #Calibrate the heat loss coefficient and store the results.

    for i in range(0,200):
        outputs_test = outputs.iloc[-i,:].values
        y_cal = ss_out.transform(outputs_test.reshape(-1,1))
        model_cali = modify_model(model, (len(ss.mean_),1))
        model_cali.fit(np.ones((1,len(ss.mean_),1)), y_cal.reshape(1,1,-1), epochs=6000, verbose=False)
        results.append([model_cali.layers[0].weights[0][w].numpy()[0][0] for w in range(len(ss.mean_))])
            
    pd.DataFrame(np.array(results)).to_csv('backprop_results_'+path+'.csv')

    

""
                    ###Class to perform all calibration steps###
""


class Surrogate_calibration()
    def __init__(self, file_name):
        '''
        file_path: Defines the file name of the simulation data collected for one of the 16 building cases.
        '''
        self.file_path = file_name
        
    def train_surrogate(self):        
        self.model = fit_surrogate(self.file_path)
        
    def perform_optimization_based_calibration(self):
        '''Performs GA/Backpropagation-based calibration and stores the results.'''
        run_calibration(self.file_path)
        
    def perform_Bayesian_calibration(self):
        run_bayesian_calibration(self.file_path)


