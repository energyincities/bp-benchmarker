import os, pdb

import torch
from torch import nn
import pandas as pd

from fastai.text import BatchNorm1dFlat

from GrayBoxMethod import GrayBoxMethod
from BuildingDataset import BuildingDataset


# # ===================================
# # RNN
# # ======================================

class RNNRegression(nn.Module):
    def __init__(self, nv=1, nh=80, out=1, layers=3, ni=4, use_cuda=True):
        super().__init__()
        self.use_cuda= use_cuda
        self.l_in = nn.Linear(ni, nh)
        self.l_hidden = nn.GRU(nh, nh, layers, batch_first=True)
        self.l_out_1 = nn.Linear(nh, out) 
        self.bn = BatchNorm1dFlat(nh)
        self.layers = layers
        self.nh = nh
        
    def forward(self, x):
        x1 = x
        # x1 = x.transpose(2, 1)
        t = self.l_in(x1)
        if self.use_cuda:
            res, hi = self.l_hidden(
                t, torch.zeros(self.layers, t.shape[0], self.nh).cuda())
        else:
            res, hi = self.l_hidden(
                t, torch.zeros(self.layers, t.shape[0], self.nh))
        t1 = self.bn(res[:, -1, :])
        out = self.l_out_1(t1)
        return out.squeeze()


# # ===================================
# # RESNET
# # ======================================

class ResNet(nn.Module):
    def __init__(self, ni, block, layers, num_classes=1000):
        self.inplanes = 64
        super(ResNet, self).__init__()
        self.conv1 = nn.Conv1d(ni, 64, kernel_size=3, stride=3, padding=2,
                               bias=False)
        self.bn1 = nn.BatchNorm1d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool1d(kernel_size=10, stride=3, padding=2)
        self.layer1 = self._make_layer(block, 124, layers[0], stride=3)
        self.layer2 = self._make_layer(block, 256, layers[1], stride=3)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 124, layers[3], stride=2)
        self.avgpool = nn.AvgPool1d(7, stride=1)
        self.fc = nn.Linear(124 * block.expansion, num_classes)
        
    def forward(self, x):
        x = torch.transpose(x, 1, 2)
        x = self.conv1(x) 
        x = self.bn1(x)     
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.layer1(x)
        x = self.layer2(x) 
        x = self.layer3(x)  
        x = self.layer4(x)  

        x = self.avgpool(x)  
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        
        return torch.flatten(x)
    
    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv1d(self.inplanes, planes * block.expansion,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm1d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, ni, planes, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv1d(ni, planes, stride=stride, kernel_size=3, padding=1)
        self.bn1 = nn.BatchNorm1d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv1d(planes, planes, kernel_size=3, padding=1)
        self.bn2 = nn.BatchNorm1d(planes)
        self.downsample = downsample
        self.stride = stride
        
    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)
        out += residual
        out = self.relu(out)

        return out


# # ===================================
# # Saver
# # ======================================

class SaveBlackBox():
    label_idxs = {
        0: 'envelope_hlc',
        1: 'envelope_umat',
        2: 'envelope_use',
        3: 'envelope_usi',
        4: 'hlc_inf',
        5: 'envelope_inf_hlc',   
    }
    
    def __init__(self, label_idx, network, run_count, valid_fname, 
                 model_dir, use_cuda=True, save_dir=None):
        #         self.save_prefix = network
        #         self.network = network
        self.use_cuda = use_cuda
        self.network  = network
        self.ylabel = self.label_idxs[label_idx]
        self.valid_fname = valid_fname
        self.save_dir = save_dir
        self.ds = BuildingDataset(valid_fname, label_idx)
        self.model_fname = str(label_idx) + '_' + network + '_' + str(run_count)
        self.mpath = os.path.join(model_dir, self.model_fname)
        self.load_model()
      
    def case_string(self, case):
        case_name = ''
        for c in case:
            case_name += c #+ '\n'
        return case_name
    
    def resnet(self, ni, **kwargs):
        model = ResNet(ni, BasicBlock, [3, 4, 6, 3, 3], **kwargs)
        return model
        
    def load_model(self):
        if self.network == 'cnn':
            m = self.resnet(ni=4, num_classes=1)
        elif self.network == 'rnn':
            m = RNNRegression(ni=4, use_cuda=self.use_cuda)
            
        if self.use_cuda:
            state = torch.load(self.mpath)
            self.model = m.cuda()
        else:
            state = torch.load(self.mpath, map_location=torch.device('cpu'))
            self.model = m
            
        self.model.load_state_dict(state)
        self.model.eval()
        
    def __call__(self, case):
#         print('Saving Neural Network Results For case', case)
        case_name = self.case_string(case)
        save_name = f'{case[0]}_{case[1]}_sched{case[2]}'
        (x, y), md = self.ds.get_by_metadata(list(case))
        if self.use_cuda:
            preds = self.model(x.cuda()).tolist()
        else:
            preds = self.model(x).tolist()
        df_results = pd.DataFrame({
            self.ylabel: y,
            self.ylabel + '_pred': preds,
            'md': md.tolist()
        })
        save_dir = os.path.join(self.save_dir, self.model_fname + f'_{save_name}.csv')
        if not os.path.exists(self.save_dir): os.mkdir(self.save_dir)
        df_results.to_csv(save_dir, index=False)
