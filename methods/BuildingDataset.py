# +
import pdb
import torch
from torch import tensor
from torch.utils.data import Dataset, DataLoader
import h5py

from sklearn.preprocessing import MinMaxScaler

# +
def get_training_scalers(vals, feature_range=(0, 1)):
    assert vals.dtype == float

    scalers = {}
    for i in range(vals.shape[1]):
        scalers[i] = MinMaxScaler(feature_range=feature_range)
        reshaped = vals[:, i, :].reshape(-1)
        reshaped = reshaped.reshape(len(reshaped), 1)
        scalers[i].fit(reshaped)
    return scalers

def normalize(vals, scalers):
    vals_new = vals.copy()
    for i in range(vals.shape[1]):
        for j in range(vals[:, i, :].shape[0]):
            t = scalers[i].transform(vals[j, i, :].reshape(len(vals[j, i, :]), 1)).reshape(-1)
            vals_new[j,i,:] = t

    return vals_new


# -

class BuildingDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, hd5_file, label_idx, transform=False, scalers=None):
        """
        Label Indices:
        {
            0: 'envelope_hlc',
            1: 'envelope_umat',
            2: 'envelope_use',
            3: 'envelope_usi',
            4: 'hlc_inf',
            5: 'envelope_inf_hlc',   
        }
        """

        self.hd5_file  = hd5_file
        self.label_idx = label_idx
        self.transform = transform
        if transform and not scalers: 
            print('getting scalers')
            self.scalers = self.get_scalers()
        
        
    @property
    def x(self):
        with h5py.File(self.hd5_file, 'r') as f:
            X = f['ts'][:,:,:]
        return tensor(X).float()
    
    @property
    def y(self):
        with h5py.File(self.hd5_file, 'r') as f:
            y = f['labels'][:,self.label_idx]
        return tensor(y).float()

    def __len__(self):
        with h5py.File(self.hd5_file, 'r') as f:
            return len(f['ts'])

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        with h5py.File(self.hd5_file, 'r') as f:
            X = f['ts'][idx,:,:]
            y = f['labels'][idx, self.label_idx]

        if self.transform:
            return tensor(normalize(X, self.scalers)).float(), tensor(y).float()
        else:
            return tensor(X).float(), tensor(y).float()
    
    def get_scalers(self):
        with h5py.File(self.hd5_file, 'r') as f:
            X = f['ts'][:,:,:]
            scalers = get_training_scalers(X, feature_range=(0.001, 1))
        return scalers
    
    @staticmethod
    def sublist(lst1, lst2):
        ls1 = [element for element in lst1 if element in lst2]
        ls2 = [element for element in lst2 if element in lst1]
        return ((ls1 == ls2) and (len(ls1) == len(lst1)))
    
    def get_by_metadata(self, case):
        case_enc = case.copy()
        for i, r in enumerate(case):
            case_enc[i] = r.encode("ascii", "ignore")

        with h5py.File(self.hd5_file, 'r') as f:
            md = f['md'][:]
            
        t = [self.sublist(case_enc, x) for x in md]
        idxs = [i for i, x in enumerate(t) if x]

        return self[idxs], md[idxs]

class DataBunch():
    def __init__(self, train_dl, valid_dl):
        self.train_dl,self.valid_dl = train_dl,valid_dl

    @property
    def train_ds(self): return self.train_dl.dataset

    @property
    def valid_ds(self): return self.valid_dl.dataset

def get_dls(train_ds, valid_ds, bs, **kwargs):
    return (DataLoader(train_ds, batch_size=bs, shuffle=True, **kwargs),
            DataLoader(valid_ds, batch_size=bs*2, **kwargs))

