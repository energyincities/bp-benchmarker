import os, sqlite3, pdb

import pandas as pd
from tqdm.notebook import tqdm

from eplussql import index_time_series


class EPlusResults():
    def __init__(self, rootdir, idf, rate):
        """ The rate is the resample rate. The idf is the name of the base idf ex. control """
        self.rootdir = rootdir
        self.idf     = idf
        self.rate    = rate
        self.surfacevars = self.list2EPlus([
            'Surface Outside Face Convection Heat Transfer Coefficient', 
            'Surface Outside Face Thermal Radiation to Sky Heat Transfer Coefficient', 
            'Surface Outside Face Thermal Radiation to Air Heat Transfer Coefficient', 
            'Surface Inside Face Convection Heat Transfer Coefficient',
            'Surface Outside Face Thermal Radiation to Ground Heat Transfer Coefficient'
        ])
         
    @staticmethod
    def select_surface_constructions(conn):
        q = "SELECT S.SurfaceName, C.Name, C.Uvalue, S.Area, S.ClassName FROM Surfaces as S " \
            "JOIN Constructions as C on S.ConstructionIndex == C.ConstructionIndex"
        return pd.read_sql(q, conn)
       
    @staticmethod
    def select_time_series(conn, reporting_frequency, keyvals=None, names=None):
        s = 'Value, Name, IndexGroup, KeyValue, Units, Year, Month, Day, Hour, Minute'
        q = f"SELECT {s} FROM ReportData as RD " \
            "JOIN ReportDataDictionary as RDD on RD.ReportDataDictionaryIndex = RDD.ReportDataDictionaryIndex " \
            "JOIN Time as T on T.TimeIndex = RD.TimeIndex " \
            "AND RDD.ReportingFrequency == '{}' "
        if keyvals:       q = q + "AND KeyValue IN ({}) "  .format(keyvals)
        if names:         q = q + "AND Name IN ({}) ".format(names)

        q = q.format(reporting_frequency)
        df = pd.read_sql(q, conn)
        df.index = index_time_series(df, reporting_frequency)
        df.Value = pd.to_numeric(df.Value)

        return df[['Units', 'Value', 'KeyValue', 'Name']]
    
    @staticmethod
    def list2EPlus(l):
        tup = ''
        for x in l: tup += f"'{x}', "
        return tup[:-2]
    
    
    def heat_loss_coefficient(self, conn, surfaces, surface_df):
        surfacevals = self.list2EPlus(surfaces)
        ts = self.select_time_series(conn, 'Zone Timestep', keyvals=surfacevals, names=self.surfacevars)
        hlcsum, usum, areasum, umatsum, usesum, usisum = 0, 0, 0, 0, 0, 0
        for surface in surfaces:
            surface_ts = ts[ts['KeyValue'] == surface]
            surface_props = surface_df[surface_df['SurfaceName'] == surface]
            area = surface_props['Area'].values[0]
            
            if not surface_props['ClassName'].values[0] == 'Floor':

                # Get the R value from the materials only
                rmat = (1/surface_props['Uvalue']).values[0]

                # get external surface R value
                htc_conv    = surface_ts[surface_ts['Name'] == 'Surface Outside Face Convection Heat Transfer Coefficient']['Value']
                htc_rad_sky = surface_ts[surface_ts['Name'] == 'Surface Outside Face Thermal Radiation to Sky Heat Transfer Coefficient']['Value']
                htc_rad_air = surface_ts[surface_ts['Name'] == 'Surface Outside Face Thermal Radiation to Air Heat Transfer Coefficient']['Value']
                htc_rad_grd = surface_ts[surface_ts['Name'] == 'Surface Outside Face Thermal Radiation to Ground Heat Transfer Coefficient']['Value']
                rse = 1/(htc_conv + htc_rad_sky + htc_rad_air + htc_rad_grd)

                # get internal surface R value
                htc_conv_in = surface_ts[surface_ts['Name'] == 'Surface Inside Face Convection Heat Transfer Coefficient']['Value']
                rsi = 1/(htc_conv_in)
                areasum += area
                umatsum += area*(1/rmat)
                usesum  += area*(1/rse).mean()
                usisum  += area*(1/rsi).mean()
            else:
                # The floor is adiabatic, so only consider the inside temperature exchange
                htc_conv_in = surface_ts[surface_ts['Name'] == 'Surface Inside Face Convection Heat Transfer Coefficient']['Value']
                rsi = 1/(htc_conv_in)
                
                areasum += area
                usisum  += area*(1/rsi).mean()
            
            
        return areasum, umatsum, usesum, usisum
        

    def load(self):
        rows = []
        for d in tqdm(os.listdir(self.rootdir)):
            row = {}
            if self.idf in d:
                row['fname'] = d
                sql_path = os.path.join(self.rootdir, d, 'eplusout.sql')
                conn = sqlite3.connect(sql_path)
                surface_df = self.select_surface_constructions(conn)
                
                envelope_surfaces = surface_df['SurfaceName'].values
                area, umat, use, usi = self.heat_loss_coefficient(conn, envelope_surfaces, surface_df)
                
                row['envelope_area']    = area
                row['envelope_umat']    = umat
                row['envelope_use']     = use
                row['envelope_usi']     = usi
                
                # TODO later may want to another set of terms for ground
                
                rows.append(row)
        self.results = pd.DataFrame(rows)
     
    
    def save(self, savepth):
        self.results.to_csv(savepth, index=False)
