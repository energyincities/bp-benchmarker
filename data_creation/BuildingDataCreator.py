import os, re, sys, traceback, pdb, time
# from time import time
from shutil import copyfile
from eplussql import *
from besos import eppy_funcs as ef
from besos.problem import EPProblem
from besos.evaluator import EvaluatorEP
from besos import sampling
from besos.parameters import expand_plist

from besos.parameters import expand_plist, RangeParameter, FieldSelector, Parameter, CategoryParameter


# def calculate_r(sql_path):
#     r_kw_col   = 'Surface Rvalue [K/W]'
#     r_m2kw_col = 'Construction Rvalue [m^2K/W]'

#     def select_surface_constructions(sql_path):
#         conn = sqlite3.connect(sql_path)
#         q = "SELECT S.SurfaceName, C.Name, C.Uvalue, S.Area, S.ClassName FROM Surfaces as S " \
#             "JOIN Constructions as C on S.ConstructionIndex == C.ConstructionIndex"
#         return pd.read_sql(q, conn)

#     surfaces = select_surface_constructions(sql_path)
#     surfaces[r_m2kw_col] = 1/surfaces['Uvalue']
#     surfaces[r_kw_col] = surfaces['Construction Rvalue [m^2K/W]'] / surfaces['Area']
#     return surfaces

class ControllerFromInputs():
    def __init__(self, finputs, controller, **kwargs):
        self.inputs = pd.read_csv(finputs)
        self.params = controller.params
        self.sampletype = controller.sampletype
        self.update_params()
        
    def update_params(self):
        cols = self.inputs.columns
        for c in cols:
            for p in self.params:
                if c == p.name:
                    p.value_descriptor = CategoryParameter(options=self.inputs[c].values)


class BuildingDataCreator():
    def __init__(self, idfdir, idffile, savedir, buildingData, run_surfaces, add_schedules, ep_path,
                schedule_dir='schedules', schedule_suffix='',  
                epw='CAN_BC_Victoria.717990_CWEC.epw', outdir='outputdir'):
        self.idffile         = idffile
        self.schedule_dir    = schedule_dir
        self.schedule_suffix = schedule_suffix
        self.idfpth          = os.path.join(idfdir,idffile)
        self.savedir         = savedir
        self.epw             = epw
        self.outdir          = outdir
        self.ep_path         = ep_path
        self.add_schedules   = add_schedules
        self.objectives      = ['Electricity:Facility']
        self.sqlfile         = os.path.join(self.outdir, 'eplusout.sql')     
        self.fname_itator    = self.current_fname_itator()
        self.building        = ef.get_building(self.idfpth, ep_path=self.ep_path)
        self.buildingData    = buildingData(self.sqlfile, run_surfaces=run_surfaces)
        self.prep_output_vars()
    
    
    def add_schedule_file(self, it=None):
        # TODO may not want to hardcode schedule directory...
#         pdb.set_trace()
        occ_dir   = os.path.join(self.schedule_dir, 'occupancy' + self.schedule_suffix)
        equip_dir = os.path.join(self.schedule_dir, 'equipment' + self.schedule_suffix)
        if self.add_schedules:
            if it is None: it = self.current_fname_itator()
            sched_files = self.building.idfobjects['SCHEDULE:FILE']
            schedule_filenames = []
            for s in sched_files:
                if s.Name == 'Occ Stochastic':
                    s.File_Name = os.path.join(occ_dir, f'sched_{it}.csv')
                if s.Name == 'Equip Stochastic':
                    s.File_Name = os.path.join(equip_dir, f'sched_{it}.csv')
                schedule_filenames.append(s.File_Name)
            return schedule_filenames
            
            
    def run_from_inputs_file(self, fname, paramController, job_id=None, tasks_per_job=None, numsamples=None):
        """ num_samples: number of sampels to create if the inputs file does not already exist"""
        paramController = paramController(self.building)
        problem = EPProblem(paramController.params, ['Electricity:Facility'])
        if os.path.exists(fname):
            inputs = pd.read_csv(fname).iloc[(job_id-1)*tasks_per_job:job_id*tasks_per_job,:]
            print(f'Loaded {len(inputs)} inputs.')
        else:
            inputs = sampling.dist_sampler(
                paramController.sampletype, 
                problem, 
                num_samples=numsamples)
            inputs.to_csv(fname, index=False)
        self.run_each_input_in_list(inputs, problem, it=job_id)
    
    
    def run_from_inputs_file_synch(self, fname, paramController, numsamples=None):
        """ num_samples: number of sampels to create if the inputs file does not already exist"""
        paramController = paramController(self.building)
        problem = EPProblem(paramController.params, ['Electricity:Facility'])
        if os.path.exists(fname):
            inputs = pd.read_csv(fname)
        else:
            inputs = sampling.dist_sampler(
                paramController.sampletype, 
                problem, 
                num_samples=numsamples)
            inputs.to_csv(fname, index=False)
        self.run_each_input_in_list(inputs, problem)

        
    
    def run_from_inputs(self, create_from, paramControllerOG):
        """ Run from a directory of existing buildings """
        pattern = '(.+)_\d+'
        idffile = re.match(pattern, os.listdir(create_from)[0]).group(1)
        its = self.get_iterators(create_from, idffile)
    
        for it in its:
            d = f'{idffile}_{it}'
            print(d)
            s = time.time()
            finputs = os.path.join(create_from, d, 'inputs.csv')
            print(finputs)
            paramController = ControllerFromInputs(finputs, paramControllerOG(self.building))

            problem = EPProblem(paramController.params, ['Electricity:Facility'])
            inputs = sampling.dist_sampler(
                    paramController.sampletype, 
                    problem, 
                    num_samples=2)
            selected_input = inputs.iloc[[0]] 
            schedule_filenames = self.add_schedule_file(it-1)
            e = EvaluatorEP(
                problem, 
                self.building, out_dir=self.outdir, epw_file=self.epw, ep_path=self.ep_path)
            try:
                e.df_apply(selected_input, keep_input=True)
            except Exception as exp:
                print('EnergyPlus error. Skipping example', exp)
                traceback.print_exc()
                continue
            # This will only run if there was not an energy plus error
            self.save_run_from_input(d, selected_input, schedule_filenames=schedule_filenames)
            print('     ran example', d, 'in', time.time() - s)
        
            
    def run_many(self, numsamples, paramController):
        paramController = paramController(self.building)
        problem = EPProblem(paramController.params, ['Electricity:Facility'])
        inputs = sampling.dist_sampler(
            paramController.sampletype, 
            problem, 
            num_samples=numsamples)
        self.run_each_input_in_list(inputs, problem)
            
    
    def run_each_input_in_list(self, inputs, problem, it=None):
        print('Running for each sample...')
        for i in range(len(inputs)):
            s = time.time()
            selected_input = inputs.iloc[[i]] 
            try:
                schedule_filenames = self.add_schedule_file(it=it)
                e = EvaluatorEP(
                   problem, 
                   self.building, out_dir=self.outdir, epw_file=self.epw, ep_path=self.ep_path)
                e.df_apply(selected_input, keep_input=True)
            except Exception as exp:
                print('EnergyPlus error. Skipping example', exp)
                traceback.print_exc()
                continue
            # This will only run if there was not an energy plus error
            self.save_run(selected_input, schedule_filenames=schedule_filenames)
            print('     ran example', self.fname_itator, 'in', time.time() - s)
        
    
    def run_one(self):
        """ Run a single example without changing any params """
        st = time.time()
        print('Running EPlus...')
        e = EvaluatorEP(
            EPProblem(outputs=self.objectives), 
            self.building, out_dir=self.outdir, epw_file=self.epw, ep_path=self.ep_path)
        e([]); 
        et = time.time()
        print(f'Running EnergyPlus took {et - st} seconds')
        st = time.time()
        print('Saving results...')
        self.save_run()    
        et = time.time()
        print(f'Saving results took {et - st} seconds')
        
        
    def save_run_from_input(self, fnameog, inputs=None, schedule_filenames=None):
        """ inputs will be a pandas dataframe if run_many is called """
        fname_itator = re.findall('.+_(\d+)$', fnameog)[0]
        savedir = os.path.join(self.savedir, self.idffile[:-4] + f'_{fname_itator}')
        self.buildingData.load()                         # Load data first incase there is an error
        self.buildingData.save(savedir)                  # This will make the directory if it doesn't exist already
        self.backup_sql(savedir)
        if inputs is not None: inputs.to_csv(os.path.join(savedir, 'inputs.csv'), index=False)
        self.save_schedules(savedir, schedule_filenames)
            

    def save_run(self, inputs=None, schedule_filenames=None):
        """ inputs will be a pandas dataframe if run_many is called """
        self.buildingData.load()           # Load data first incase there is an error
        savedir = self.prep_save()         # Get the next file name based on iterator
        self.buildingData.save(savedir)    # This will make the directory if it doesn't exist already
        # self.save_thermal_props(savedir)
        self.backup_sql(savedir)
        if inputs is not None: inputs.to_csv(os.path.join(savedir, 'inputs.csv'), index=False)
        self.save_schedules(savedir, schedule_filenames)
    
    
    def get_iterators(self, data_dir, idffile):
        def get_it(string):
            pattern = '.+_(\d+)$'
            result = re.match(pattern, string)
            return result.group(1)
        try:
            ds = os.listdir(data_dir)
        except FileNotFoundError:
#             print(f'{self.savedir} Does not exist yet. It will be created for the first time.')
            ds = []
        houses = [x for x in ds if idffile[:-4] in x]
        if not houses: return []
        its = []
        for house in houses:
            it = get_it(house)
            its.append(int(it))
        return its
    
    
    def current_fname_itator(self):
        """ Find the most recent file number that was created """
        its = self.get_iterators(self.savedir, self.idffile)
        if not its: return 0
        return max(its)
     
    def prep_output_vars(self):
        for output in self.buildingData.eplus_output_vars:
            out_var = self.building.newidfobject('OUTPUT:VARIABLE')
            out_var.Variable_Name = output
            out_var.Reporting_Frequency = 'Timestep'
        
        if self.add_schedules:
            ### Occupancy
            out_var = self.building.newidfobject('SCHEDULE:FILE')
            out_var.Name                          = 'Occ Stochastic'
            out_var.Column_Number                 = 2
            out_var.Rows_to_Skip_at_Top           = 1
            out_var.Minutes_per_Item              = 5
            # out_var.Interpolate_to_Timestep   = 'Yes'
            # out_var.Schedule_Type_Limits_Name = 'Fraction'
            p = self.building.idfobjects['PEOPLE'][0]
            p.Number_of_People_Schedule_Name      = 'Occ Stochastic'
            p.Number_of_People_Calculation_Method = 'People'
            p.Number_of_People                    = 1
            
            #### Equipment
            out_var = self.building.newidfobject('SCHEDULE:FILE')
            out_var.Name                          = 'Equip Stochastic'
            out_var.Column_Number                 = 2
            out_var.Rows_to_Skip_at_Top           = 1
            out_var.Minutes_per_Item              = 5
            e = self.building.idfobjects['ELECTRICEQUIPMENT'][0]
            e.Schedule_Name                       = 'Equip Stochastic'
            e.Design_Level_Calculation_Method     = 'EquipmentLevel'
            e.Design_Level                        = 1
            
    def backup_sql(self, savedir):
        pth = os.path.join(savedir, 'eplusout.sql')
        copyfile(self.sqlfile, pth)
        
    def save_schedules(self, savedir, schedule_filenames):
        if schedule_filenames is not None:
            with open(os.path.join(savedir, "schedule_filenames.txt"), "w") as output:
                output.write(str(schedule_filenames))
        
    def prep_save(self):
        """ Create director to save this run """
        self.fname_itator += 1
        return os.path.join(self.savedir, self.idffile[:-4] + f'_{self.fname_itator}')


#     def save_thermal_props(self, savedir):
#         # For now only save R because C is more complicated to determine 
#         # therefore its harder to compare with the other methods and "ground truth"
#         df = calculate_r(self.sqlfile)
#         df.to_csv(os.path.join(savedir, 'thermal_props.csv'))





