#!/bin/bash
#SBATCH --account=rpp-revins

#SBATCH --array=1-800
#SBATCH --time=0:10:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=2000M
#SBATCH --mail-user=gbaasch@uvic.ca
#SBATCH --mail-type=ALL


#!generate the virtual environment
module load python/3.6

source ~/envs/dl3.6/bin/activate

echo "prog started at: `date`"
echo "task: `$SLURM_ARRAY_TASK_ID`"

srun python generate_schedules.py -task_id $SLURM_ARRAY_TASK_ID -root /scratch/gbaasch/schedules -suffix _train

deactivate
echo "prog ended at: `date`"
