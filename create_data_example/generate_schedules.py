import argparse, os

import numpy as np
import pandas as pd

from tqdm.notebook import tqdm

import richardsonpy.classes.occupancy as occ

import richardsonpy.classes.occupancy as occ
import richardsonpy.functions.change_resolution as cr
import richardsonpy.functions.load_radiation as loadrad
import richardsonpy.classes.electric_load as eload

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('-task_id', type=int, nargs='+',
                   help='Task id to pick inputs')
parser.add_argument('-root', type=str, nargs='+', help='Root directory for the schedules')
parser.add_argument('-suffix', type=str, nargs='+', help='Suffix name for the subdirectories')
args = parser.parse_args()
task_id = args.task_id[0]
root = args.root[0]
suffix = args.suffix[0]

occ_root = os.path.join(root, 'occupancy' + suffix)
equip_root = os.path.join(root, 'equipment' + suffix)

if not os.path.exists(occ_root):
    os.mkdir(occ_root)
if not os.path.exists(equip_root):
    os.mkdir(equip_root)


timestep = 300  # in seconds

idx = pd.DatetimeIndex(pd.date_range(start = '01.01.2006 00:00:00', end = '31.12.2006 23:55:00', freq='5min'))

def get_rad():
    df = pd.read_csv('solar.csv')
    return df['direct_rate'].values, df['diffuse_rate'].values

#  Get radiation (necessary for lighting usage calculation)
(q_direct, q_diffuse) = get_rad()

def save_profiles(i):
    #  Generate occupancy object (necessary as input for electric load gen.)
    nb_occ = np.random.randint(low=1, high=5) # this will produce a number between 1 and 4
    occ_obj  = occ.Occupancy(number_occupants=nb_occ)
    
    occ_profile = cr.change_resolution(values=occ_obj.occupancy,
                                           old_res=600,
                                           new_res=timestep)



    #     #  Generate stochastic electric load object instance
    demand = np.random.normal(loc=250, scale=100)
    el_load_obj = eload.ElectricLoad(occ_profile=occ_obj.occupancy,
                                         total_nb_occ=nb_occ,
                                         q_direct=q_direct,
                                         q_diffuse=q_diffuse,
                                         annual_demand=demand,
                                         timestep=timestep)

    #     #  Calculate el. energy in kWh by accessing loadcurve attribute
    #     energy_el_kwh = sum(el_load_obj.loadcurve) * timestep / (3600 * 1000)

    df_occ  = pd.DataFrame(occ_profile, index=idx)
    df_elec = pd.DataFrame(el_load_obj.loadcurve, index=idx)
    df_occ.to_csv(os.path.join(occ_root, f'sched_{i}.csv'))
    df_elec.to_csv(os.path.join(equip_root, f'sched_{i}.csv'))


save_profiles(task_id)
