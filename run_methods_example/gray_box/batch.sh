#!/bin/bash
#SBATCH --account=rpp-revins

#SBATCH --array=0-15
#SBATCH --time=00:45:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=2000mb
#SBATCH --mail-user=gbaasch@uvic.ca
#SBATCH --mail-type=ALL


# prepare R
export R_LIBS=~/local/R_libs/
module load gcc/7.3.0 r/3.6.0

# load the virtual environment
source ~/envs/dl3.6/bin/activate


echo "prog started at: `date`"


case $SLURM_ARRAY_TASK_ID in
    0) ARGS="-epw victoria -idf control_concrete -sched True" ;;
    1) ARGS="-epw victoria -idf control_concrete -sched False" ;;
    2) ARGS="-epw victoria -idf control_wood -sched True" ;;
    3) ARGS="-epw victoria -idf control_wood -sched False" ;;
    4) ARGS="-epw victoria -idf control_concrete_inf -sched True" ;;
    5) ARGS="-epw victoria -idf control_concrete_inf -sched False" ;;
    6) ARGS="-epw victoria -idf control_wood_inf -sched True" ;;
    7) ARGS="-epw victoria -idf control_wood_inf -sched False" ;;
    8) ARGS="-epw chicago -idf control_concrete -sched True" ;;
    9) ARGS="-epw chicago -idf control_concrete -sched False" ;;
    10) ARGS="-epw chicago -idf control_wood -sched True" ;;
    11) ARGS="-epw chicago -idf control_wood -sched False" ;;
    12) ARGS="-epw chicago -idf control_concrete_inf -sched True" ;;
    13) ARGS="-epw chicago -idf control_concrete_inf -sched False" ;;
    14) ARGS="-epw chicago -idf control_wood_inf -sched True" ;;
    15) ARGS="-epw chicago -idf control_wood_inf -sched False" ;;
esac

srun python run_all.py $ARGS -dataset_path /scratch/gbaasch/bp_benchmark_data/test
