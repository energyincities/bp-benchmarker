import os, subprocess, argparse, sys
import matplotlib.pyplot as plt

import pandas as pd

sys.path.append('../../')
from methods.GrayBoxMethod import BalancePointMethod, DecayCurveMethod, RCMethod

# data directory
# ROOT   = '/home/gbaasch/projects/def-revins/gbaasch/thermal_property_calibration/datasets_paper/datasets_infiltration/'

# TODO - add support for this later
cols   = ['outtemp', 'total_s']                               # input variables

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('-epw', type=str, nargs='+')
parser.add_argument('-idf', type=str, nargs='+')
parser.add_argument('-sched', type=str, nargs='+')
parser.add_argument('-mOrder', type=str, nargs='+')
parser.add_argument('-dt', type=str, nargs='+')
parser.add_argument('-dataset_path', type=str, nargs='+')
args = parser.parse_args()

ROOT = args.dataset_path[0]
dt = args.dt[0]
mOrder = args.mOrder[0]

dataset  = args.epw[0]+ '_' + args.idf[0] + '_sched' + args.sched[0]
print('Running', dataset)

## == Run RC Method =====
try:
    rc = RCMethod(
        ROOT,
        dataset,
        args.idf[0],
        dt,
        None,
        mOrder, 
        'rc_results', 
        '../../methods/rc_methods/experiments',
        '../../methods/rc_methods/'
    )
    rc();
except Exception as e:
    print(e)
print('COMPLETE')


