import sys, os, subprocess, argparse
import matplotlib.pyplot as plt

import pandas as pd


sys.path.append('../../')
from methods.GrayBoxMethod import BalancePointMethod, DecayCurveMethod, RCMethod

# data directory
#ROOT   = '/home/gbaasch/projects/def-revins/gbaasch/thermal_property_calibration/datasets_paper/datasets_infiltration/'


# TODO - add support for additional of these later
cols   = ['outtemp', 'total_s']                               # input variables

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('-epw', type=str, nargs='+')
parser.add_argument('-idf', type=str, nargs='+')
parser.add_argument('-sched', type=str, nargs='+')
parser.add_argument('-dataset_path', type=str, nargs='+')
args = parser.parse_args()

ROOT = args.dataset_path[0]


dataset  = args.epw[0]+ '_' + args.idf[0] + '_sched' + args.sched[0]
print('Running', dataset)
## == Run Greybox Methods =====
bp  = BalancePointMethod(ROOT, dataset, args.idf[0], 'D', cols)
dc  = DecayCurveMethod(ROOT,   dataset, args.idf[0], '5T', cols)
#rc = RCMethod(
#    ROOT,
#    dataset,
#    args.idf[0],
#    'dt_jan7_5',
#    None,
#    'Ti2', 
#    '../rc_results', 
#    '../../run_rc_methods/experiments',
#    '../../run_rc_methods/'
#)
bp();
print('BP COMPLETE')
dc(); 
print('DC COMPLETE')
#rc();
print('COMPLETE')
            
