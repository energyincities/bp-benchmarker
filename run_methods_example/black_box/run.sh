#!/bin/bash

#SBATCH --account=def-revins

#SBATCH --array=0-5
#SBATCH --time=06:00:00
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH --mem=2000mb
#SBATCH --mail-user=gbaasch@uvic.ca
#SBATCH --mail-type=ALL


# prepare R
export R_LIBS=~/local/R_libs/
module load gcc/7.3.0 r/3.6.0

# load the virtual environment
source ~/envs/dl3.6/bin/activate


echo "prog started at: `date`"


case $SLURM_ARRAY_TASK_ID in
    0) ARGS="-network cnn -label_idx 0" ;;
    1) ARGS="-network cnn -label_idx 1" ;;
    2) ARGS="-network cnn -label_idx 5" ;;
    3) ARGS="-network rnn -label_idx 0" ;;
    4) ARGS="-network rnn -label_idx 1" ;;
    5) ARGS="-network rnn -label_idx 5" ;;
esac

srun python execute_papermill.py $ARGS -train_fname ../../hdf5/train.hdf5 -valid_fname ../../hdf5/test.hdf5
